<div class="product-detail-layout-3">
<?php
global $post;
$flooringtype = $post->post_type; 
$meta_values = get_post_meta( get_the_ID() );
$brand = $meta_values['brand'][0] ;
$sku = $meta_values['sku'][0];
$manufacturer = $meta_values['manufacturer'][0];
$image = swatch_image_product(get_the_ID(),'600','400');
$getcouponbtn = get_option('getcouponbtn');
$getcouponreplace = get_option('getcouponreplace');
$getcouponreplacetext = get_option('getcouponreplacetext');
$getcouponreplaceurl = get_option('getcouponreplaceurl');
$pdp_get_finance = get_option('pdp_get_finance');
$getfinancereplace = get_option('getfinancereplace');
$getfinancereplaceurl = get_option('getfinancereplaceurl');
$getfinancetext = get_option('getfinancetext');

if(get_option('salesbrand')!=''){
	$slide_brands = rtrim(get_option('salesbrand'), ",");
	$brandonsale = array_filter(explode(",",$slide_brands));
	$brandonsale = array_map('trim', $brandonsale);
}
  
?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
			<div class="row">
				<div class="col-md-6 col-sm-12 product-swatch">   
					<?php 
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
						include( $dir );
					?>
				</div>
				<div class="col-md-6 col-md-12 product-box">
					<?php
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
						include_once( $dir );
					?>
						<div class="clearfix"></div>
						<?php if(array_key_exists("parent_collection",$meta_values)){?>
						<h4><?php echo $meta_values['parent_collection'][0]; ?></h4>
						<?php } ?>
					<?php if(array_key_exists("collection",$meta_values)){?>
					<h2 class="fl-post-title" itemprop="name"><?php echo $meta_values['collection'][0]; ?></h2>
					<?php } ?>
					<?php if(array_key_exists("color",$meta_values)){?>
					<h1 class="fl-post-title" itemprop="name"><?php echo $meta_values['color'][0]; ?></h1>
					<?php } ?>
					<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
					<div class="product-colors">
						<?php
							$collection = $meta_values['collection'][0]; 
							if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

								$familycolor = $meta_values['style'][0];
								$key = 'style';

							}else{

								$familycolor = $meta_values['collection'][0];    
								$key = 'collection';
							}	

							$args = array(
								'post_type'      => $flooringtype,
								'posts_per_page' => -1,
								'post_status'    => 'publish',
								'meta_query'     => array(
									array(
										'key'     => $key,
										'value'   => $familycolor,
										'compare' => '='
									),
									array(
										'key' => 'swatch_image_link',
										'value' => '',
										'compare' => '!='
										)
								)
							);										
					
							$the_query = new WP_Query( $args );
						?>
						<ul>
							<li class="color-count" style="font-size:14px;"><?php echo $the_query->found_posts; ?> Colors Available</li>
						</ul>
					</div>
					<div class="button-wrapper">
						<?php  if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){  ?>
												<a href="<?php if($getcouponreplace ==1){ echo $getcouponreplaceurl ;}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
												<?php if( $getcouponreplace ==1){ echo $getcouponreplacetext ;}else{ echo 'GET COUPON'; }?>
												</a>
						<?php } ?>
						<a href="/contact-us/"  class="button contact-btn">Contact Us</a>
						
						<?php if($pdp_get_finance != 1 || $pdp_get_finance  == '' ){?>						
							<a href="<?php if($getfinancereplace ==1){ echo  $getfinancereplaceurl ;}else{ echo '/flooring-financing/'; } ?>" class="finance-btn button"><?php if($getfinancereplace =='1'){ echo $getfinancetext ;}else{ echo 'Get Financing'; } ?></a>	
						<?php } ?> 

						<?php roomvo_script_integration($manufacturer,$sku,get_the_ID());?>

					<!-- 	 <span class="divider">&nbsp;|&nbsp;</span> <a href="/schedule-appointment/">Schedule a Measurement</a> -->
					</div>
				</div>
					<div class="clearfix"></div>
			</div>
				<div class="clearfix"></div>
			<?php
				$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
				include( $dir );
			?>
				<div class="clearfix"></div>
			<div id="product-attributes-wrap">
				<?php //get_template_part('includes/product-attributes');
				$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
				include( $dir );
				?>
			</div>
		</div>
		
	</article>
</div>
<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>